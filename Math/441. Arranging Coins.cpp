class Solution {
public:
    int arrangeCoins(int n) {
        int start = 0;
        while (n >= 0) n -= ++start;
        return start - 1;
    }
};