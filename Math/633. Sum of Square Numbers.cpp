class Solution {
public:
    bool judgeSquareSum(int c) {
        for (int i = 0; i <= (int)sqrt(c); i++) {
            int rest = c - i * i;
            // if (isPerfectSquare(rest)) return true;
            if (pow((int)sqrt(rest), 2) == rest) {
                // cout << i << " " << rest << " " << sqrt(rest) << endl;
                return true;
            }
        }
        return false;
    }
    
    bool isPerfectSquare(int num) {
        if (num == 0) return true;
        int invalidation[] = {2, 3, 7, 8};
        for (int i = 0; i < sizeof(invalidation) / sizeof(int); i++) {
            if (num % 10  == invalidation[i]) return false;
        }
        int clock = 1;
        while (num > 0) {
            num -= clock;
            clock += 2;
        }
        return num == 0;
    }
};