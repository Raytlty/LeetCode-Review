/*
1. 十进制的完全平方数的末位数只能是0, 1, 4, 5, 6, 9; 
十六进制的完全平方数的末位数只能是0, 1, 4, 9

2. 除0以外的完全平方数是从1开始的连续的奇数的和
例如: 1 = 1, 4 = 1 + 3, 9 = 1 + 3 + 5, 16 = 1 + 3 + 5 + 7
*/

class Solution {
public:
    bool isPerfectSquare(int num) {
        if (num == 0) return true;
        int invalidation[] = {2, 3, 7, 8};
        for (int i = 0; i < sizeof(invalidation) / sizeof(int); i++) {
            if (num % 10  == invalidation[i]) return false;
        }
        int clock = 1;
        while (num > 0) {
            num -= clock;
            clock += 2;
        }
        return num == 0;
    }
};