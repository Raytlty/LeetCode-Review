#include <iostream>
using namespace std;


class Solution {
public:
    string convert(string s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        string result = "";
        int gap = 2 * (numRows - 1);
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j + i < (int)s.size(); j += gap) {
                result += s[i + j];
                if (!(i == 0 || i == numRows - 1)) {
                    int next = gap - 2 * i;
                    if (i + j + next < (int)s.size()) {
                        result += s[i + j + next];
                    }
                }
            }
        }
        return result;
    }
};
