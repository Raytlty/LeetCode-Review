class Solution {
public:
    int uniqueMorseRepresentations(vector<string>& words) {
        string table[] = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
        unordered_set<string> _set;
        for (int i = 0; i < words.size(); i++) {
            string temp;
            for (int j = 0; j < words[i].size(); j++) {
                int chIdx = words[i][j] - 'a';
                temp += table[chIdx];
            }
            _set.insert(temp);
        }
        return static_cast<int>(_set.size());
    }
};