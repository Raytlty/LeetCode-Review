
class Solution {
    public:
    vector<vector<string>> findDuplicate(vector<string>& paths) {
        unordered_map<string, vector<string>> path_collection;
        vector<vector<string>> ans;
        string item, root, filename, content;
        unsigned content_index;
        for (string path: paths) {
            stringstream strstream(path);
            getline(strstream, root, ' ');
            while (getline(strstream, item, ' ')) {
                content_index = item.find("(");
                filename = root + "/" + item.substr(0, content_index);
                content = item.substr(content_index + 1, item.find(")") - content_index - 1);
                path_collection[content].push_back(filename);
            }
        }

        for(auto&x: path_collection) {
            if (x.second.size() > 1)
                ans.push_back(x.second);
        }
        return ans;
    }
};