class Solution {
public:
    int numJewelsInStones(string J, string S) {
        map<char, bool> existed;
        for (int i = 0; i < J.size(); i++) {
            existed[J[i]] = true;
        }
        int total = 0;
        for (int i = 0; i < S.size(); i++) {
            if (existed[S[i]]) total++;
        }
        return total;
    }
};