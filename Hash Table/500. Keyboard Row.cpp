class Solution {
public:
    vector<string> findWords(vector<string>& words) {
        string s1 = "qwertyuiop";
        string s2 = "asdfghjkl";
        string s3 = "zxcvbnm";
        map<char, bool> existed1;
        map<char, bool> existed2;
        map<char, bool> existed3;
        for (int i = 0; i < s1.size(); i++) {
            existed1[s1[i]] = true;
        }
        for (int i = 0; i < s2.size(); i++) {
            existed2[s2[i]] = true;
        }
            
        for (int i = 0; i < s3.size(); i++) {
            existed3[s3[i]] = true;
        }
        vector<string> res;
        for (string word: words) {
            string temp = word;
            transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
            int failed = 0;
            for (int i = 0; i < word.size(); i++) if (!existed1[temp[i]]) { failed++; break;}
            for (int i = 0; i < word.size(); i++) if (!existed2[temp[i]]) { failed++; break;}
            for (int i = 0; i < word.size(); i++) if (!existed3[temp[i]]) { failed++; break;}
            if (failed < 3) res.push_back(word);
        }
        return res;
    }
};