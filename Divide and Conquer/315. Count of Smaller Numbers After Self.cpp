#include <iostream>
#include <vector>
#include <map>
using namespace std;
class Solution {
public:
    // Also use merge Sort and calculate
    map<int, int> ans; // (index, count) pair
    map<int, int> table; // (new_index, original_index)
    vector<int> countSmaller(vector<int>& nums) {
        for (int i = 0; i < (int)nums.size(); i++) {
            table[i] = i; // init
            ans[i] = 0;
        }
        mergeSort(nums, 0, nums.size() - 1);
        vector<int> res;
        for(int i = 0; i < (int)nums.size(); i++) {
            res.push_back(ans[i]);
        }
        return res;
    }
    
    void printVector(vector<int> seq, int left, int right, string type="Temp") {
        cout << type << " " << "Left: " << left << " Right: " << right << endl;
        for (int i = 0; i < (int)seq.size(); i++) {
            cout << seq[i] << " ";
        }
        cout << endl;
    }
    
    void mergeSort(vector<int>& nums, int left, int right) {
        if (left >= right) {
            return;
        }
        int middle = (left + right) / 2;
        mergeSort(nums, left, middle);
        mergeSort(nums, middle + 1, right);
        
        int ll = middle, rr = right;
        vector<int> temp;
        vector<int> index;
        while (ll >= left && rr >= middle + 1) {
            while (ll >= left && nums[ll] > nums[rr]) {
                ans[table[ll]] += rr - middle;
                temp.push_back(nums[ll]);
                index.push_back(table[ll]);
                ll--;
            }
            while (rr >= middle + 1 && nums[rr] >= nums[ll]) {
                temp.push_back(nums[rr]);
                index.push_back(table[rr]);
                rr--;
            }
        }
        // cout << ll << " " << rr << endl;
        while (ll >= left) {
            temp.push_back(nums[ll]);
            index.push_back(table[ll]);
            ll--;
        }
        while (rr >= middle + 1) {
            temp.push_back(nums[rr]);
            index.push_back(table[rr]);
            rr--;
        }
        // printVector(temp, left, right);
        // printVector(index, left, right, "Index");
        int length = temp.size() - 1;
        for(int i = 0; i < (int)temp.size(); i++) {
            nums[i + left] = temp[length - i];
            table[i + left] = index[length - i];
        }
    }
    
};
