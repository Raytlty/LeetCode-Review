#include <iostream>
using namespace std;


class Solution {
public:
    int majorityElement(vector<int>& nums) {
        /*
        * Majority element size should more than a half
        * So, sizeof(Majority) - sizeof(Other) > 0
        */
        int count = 0, majoryElem = nums[0];
        for (int i = 1; i < (int)nums.size(); i++) {
            if (majoryElem != nums[i]) {
                count--;
            }
            if (count < 0) {
                majoryElem = nums[i];
            }
            if (majoryElem == nums[i]) {
                count++;
            }
        }
        return majoryElem;
    }
};
