#include <iostream>
using namespace std;


class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        if (matrix.size() <= 0) return false;
        int row = matrix.size();
        int column = matrix[0].size();
        
        int x = 0, y = column - 1;
        while (x  < row && y >= 0) {
            if (matrix[x][y] > target) {
                y--;
            }
            else if (matrix[x][y] < target) {
                x++;
            }
            else {
                return true;
            }
        }
        return false;
    }
};
