#include <iostream>
using namespace std;


class Solution {
public:
    int maxCoins(vector<int>& nums) {
        nums.insert(nums.begin(), 1);
        nums.insert(nums.end(), 1);
        vector<vector<int>> dp(nums.size(), vector<int>(nums.size(), -1));
        return burstBallon(nums, 0, nums.size() - 1, dp);
    }
    
    int burstBallon(vector<int>& nums, int left, int right, vector<vector<int>>& dp) {
        // if (dp[left][right] >= 0)
        //     cout << "Left: "<< left << " Right: "<< right << " "<< dp[left][right] << endl;
        
        if (dp[left][right] != -1) return dp[left][right];
        for (int i = left + 1; i < right; i++) {
            dp[left][right] = max(dp[left][right],                                   nums[left] * nums[i] * nums[right] 
                                  + burstBallon(nums, left, i, dp)
                                  + burstBallon(nums, i, right, dp));
        }
        return dp[left][right] >= 0 ? dp[left][right] : 0;
    }
};
