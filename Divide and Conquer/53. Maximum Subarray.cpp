#include <iostream>
using namespace std;


class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        if (nums.size() <= 0) return 0;
        vector<int> dp(nums.begin(), nums.end());
        
        int maxs = dp[0];
        for (int i = 0; i < nums.size(); i++) {
            if (i > 0) {
                dp[i] = max(dp[i], max(dp[i - 1] + nums[i], nums[i]));
            }
            maxs = max(dp[i], maxs);
        }
        return maxs;
    }
};
