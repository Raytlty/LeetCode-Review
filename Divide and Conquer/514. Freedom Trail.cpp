#include <iostream>
using namespace std;


class Solution {
public:
    int findRotateSteps(string ring, string key) {
        const unsigned INF = numeric_limits<unsigned int>::max();
        map<unsigned, vector<unsigned>> index;
        vector<vector<unsigned>> dp(key.size(), vector<unsigned>(ring.size(), INF));

        for (unsigned i = 0; i < ring.size(); i++) {
            int idx = ring[i] - 'a';
            index[idx].push_back(i);
        }

        unsigned ringLength = ring.size();
        for (unsigned i = 0; i < key.size(); i++) {
            for (unsigned j = 0; j < ring.size(); j++) {
                if (i == 0) {
                    // INIT
                    for (unsigned x: index[key[i] - 'a']) {
                        dp[i][x] = min(x, ringLength - x);
                    }
                    break;
                }
                if (key[i] != ring[j]) continue;
                for (unsigned x: index[key[i - 1] - 'a']) {
                    unsigned r1 = (j - x + ringLength) % ringLength;
                    unsigned r2 = (x + ringLength - j); //Assume x before j
                    unsigned r3 = (x - j + ringLength) % ringLength;
                    unsigned r4 = (j + ringLength - x); // Assume j before x
                    unsigned minium = min(min(r1, r2), min(r3, r4));
                    dp[i][j] = min(dp[i][j], dp[i - 1][x] + minium);
                }
            }
        }

        unsigned res = key.size();
        unsigned minium = INF;
        for (unsigned x : index[key[res - 1] - 'a']) {
            minium = min(minium, dp[res-1][x]);
        }
        return static_cast<int>(res + minium);
    }
};



//class Solution {
//public:
    //const unsigned INF = numeric_limits<unsigned int>::max();
    //map<unsigned, vector<unsigned>> m_index;
    //unsigned m_keyLength, m_ringLength;
    //int findRotateSteps(string ring, string key) {
        //m_keyLength = key.size();
        //m_ringLength = ring.size();
        //vector<vector<unsigned>> dp(m_keyLength, vector<unsigned>(m_ringLength, INF));

        //for (unsigned i = 0; i < m_ringLength; i++) {
            //int idx = ring[i] - 'a';
            //index[idx].push_back(i);
        //}
        //for(unsigned x: index[key[0] - 'a']) {
            //dp[0][x] = min(x, m_ringLength - x);
        //}

    //}

    //unsigned DFS(vector<vector<unsigned>> &dp, string &ring, string &key, \
                 //unsigned id, unsigned idx = -1) {
        //if (idx != -1 && dp[id][idx] != INF) {
            //return dp[id][idx];
        //}

        //for (unsigned x: index[key[id] - 'a']) {

        //}
    //}
//};
