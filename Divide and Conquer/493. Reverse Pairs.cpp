#include <iostream>
using namespace std;


class Solution {
public:
    int reversePairs(vector<int>& nums) {
        return mergeSort(nums, 0, nums.size() - 1);
    }

    void printVector(vector<int> nums, int left, int right) {
        cout << "Left: " << left << " Right: " << right << endl;
        for (int i = 0; i < nums.size(); i++) {
            cout << nums[i] << " ";
        }
        cout << endl;
    }

    int mergeSort(vector<int>& nums, int left, int right) {
        if (left >= right) {
            return 0;
        }
        int middle = (left + right) / 2;
        int leftCount = mergeSort(nums, left, middle);
        int rightCount = mergeSort(nums, middle + 1, right);
        int curCount = 0;

        for(int i = left, j = middle + 1; i <= middle && j <= right + 1; i++) {
            while(j <= right && static_cast<long long>(nums[i]) > 2 * static_cast<long long>(nums[j])) j++;
            curCount += j - middle - 1;
        }

        int ll = middle, rr = right;
        vector<int> temp;
        // printVector(nums, left, right);

        while (ll >= left && rr >= middle + 1) {
            while (ll >= left && nums[ll] >= nums[rr]) { // negative number should be ok
                // int prr = middle + 1;
                // while (prr <= right &&                 //        static_cast<long long>(nums[ll]) > static_cast<long long>(nums[prr]) * 2) {
                //     prr++;
                // }
                // curCount += prr - middle; //TLE
                temp.push_back(nums[ll--]);
            }
            while (rr >= middle + 1 && nums[rr] > nums[ll]) {
                // int pll = middle;
                // while (pll >= left &&                 //       static_cast<long long>(nums[pll]) > static_cast<long long>(nums[rr]) * 2)
                //     pll--;
                // curCount += middle - pll; //TLE
                temp.push_back(nums[rr--]);
            }
        }
        while (ll >= left) temp.push_back(nums[ll--]);
        while (rr >= middle + 1) temp.push_back(nums[rr--]);
        int length = temp.size() - 1;
        for (int i = 0; i < temp.size(); i++) {
            nums[i + left] = temp[length - i];
        }
        return leftCount + curCount + rightCount;
    }
};
