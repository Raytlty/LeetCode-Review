#include <iostream>
#include <vector>
using namespace std;


int main() {
    vector<int> a(10, 0);
    for (int i = 0; i < a.size(); i++) {
        cout << a
    }
}
class Solution {
public:
    int countRangeSum(vector<int>& nums, int lower, int upper) {
        if (nums.size() <= 0) return 0;
        vector<long long> sum(nums.size() + 1, 0);
        for (int i = 1; i <= (int)nums.size(); i++) {
            sum[i] = sum[i - 1] + nums[i - 1];
        }
        // printSum(sum);
        return mergeSort(sum, 1, nums.size(), lower, upper);
    }
    
    void printSum(vector<long long> sum, int left, int right) {
        cout << "Left: " << left << " Right: " << right << endl;
        for (int i = 0; i < (int)sum.size(); i++) {
            cout << sum[i] << " ";
        }
        cout << endl;
    }
    
    int mergeSort(vector<long long>& sum, int left , int right, int lower, int upper) {
        if (left >= right) {
            // cout << left << " " << right << " " << sum[left] << " " << sum[right] << endl;
            return (sum[left] >= lower && sum[right] <= upper) ? 1 : 0;
        }
        int middle = (left + right) / 2;
        int leftCount = mergeSort(sum, left, middle, lower, upper);
        int rightCount = mergeSort(sum, middle + 1, right, lower, upper);
        
        int curCount = 0;
        int j = middle + 1, k = middle + 1;
        int ll = left, rr = middle + 1;
        for(int ll = left, rr = middle + 1; ll <= middle; ++ll) {
            while (j <= right && sum[j] - sum[ll] < lower) j++;
            while (k <= right && sum[k] - sum[ll] <= upper) k++;
            curCount += k - j;
        }
        
        vector<long long> temp;
        ll = left, rr = middle + 1;
        while (ll <= middle && rr <= right) {
            if (sum[ll] <= sum[rr]) temp.push_back(sum[ll++]);
            else temp.push_back(sum[rr++]);
        }
        while (ll <= middle) temp.push_back(sum[ll++]);
        while (rr <= middle) temp.push_back(sum[rr++]);
        // printSum(temp, left, right);
        for(int i = 0; i < (int)temp.size(); i++) {
            sum[i + left] = temp[i];
        }
        
        return leftCount + curCount + rightCount;
    }
};
