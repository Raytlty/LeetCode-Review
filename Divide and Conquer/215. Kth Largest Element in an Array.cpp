#include <iostream>
using namespace std;


class Solution {
public:
    
    int *m_ptr = nullptr;
    
    void printVector(vector<int> nums) {
        for (int x: nums) {
            cout << x << " ";
        }
        cout << endl;
    }
    
    int findKthLargest(vector<int>& nums, int k) {
        int left = 0, right = nums.size() - 1;
        quickSort(nums, left, right, k);
        // printVector(nums);
        return m_ptr == nullptr ? nums[k-1] : *m_ptr;
    }
    
    void quickSort(vector<int>& nums, int left, int right, int k) {
        if (m_ptr) return;
        if (left < right) {
            int privot = partition2(nums, left, right);
            if (k - 1 == privot) {
                m_ptr = &nums[privot];
                return;
            }
            quickSort(nums, left, privot - 1, k);
            quickSort(nums, privot + 1, right, k);
        }
    }
    
    void swap(int &a, int &b) {
        int t = a; a = b; b = t;
    }
    
    int partition1(vector<int>& nums, int left, int right) {
        int temp = nums[left];
        while (left < right) {
            while (left < right && nums[right] <= temp) right--;
            swap(nums[right], nums[left]);
            while (left < right && nums[left] > temp) left++;
            swap(nums[right], nums[left]);
        }
        return left;
    }
    
    int partition2(vector<int>& nums, int left, int right) {
        int pre = left - 1;
        int temp = nums[right];
        while (left < right) {
            if (nums[left] >= temp) {
                swap(nums[++pre], nums[left]);
            }
            left++;
        }
        swap(nums[++pre], nums[right]);
        // printVector(nums);
        return pre;
    }
};
