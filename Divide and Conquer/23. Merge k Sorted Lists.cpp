#include <iostream>
#include <queue>
#include <vector>
using namespace std;


//AC RUNTIME 33ms


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        // Use Priority Queue, make adjust and search in O(logn) + O(1)
        /*
        * Assume length of list is M, the avg number of each ListNode is N,
        * there is O(M * N * logN)
        */
        priority_queue<ListNode*, vector<ListNode*>,\
            function<bool(const ListNode*, const ListNode*)>>\
            que([](const ListNode* a, const ListNode* b){return a->val > b->val;});
        for (ListNode* node: lists) {
            if (node) {
                que.push(node);
            }
        }
        if (que.empty()) return nullptr;
        ListNode *head = que.top(); que.pop();
        ListNode *tail = head;
        while (!que.empty()) {
            if (tail->next) {
                que.push(tail->next);
            }
            tail->next = que.top(); que.pop();
            tail = tail->next;
        }
        return head;
    }
};
