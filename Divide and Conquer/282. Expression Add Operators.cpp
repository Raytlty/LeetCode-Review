#include <iostream>
using namespace std;


class Solution {
public:
    map<string, bool> existed;
    vector<string> addOperators(string num, int target) {
        vector<string> ans;
        if (num.size() <= 0) return ans;

        DFS(ans, num, 0, target, 0L, 0L, "");
        return ans;
    }

    void DFS(vector<string>& ans, string num, int idx, int target, long sum, long pre, string aim) {

        // cout << "Num: " << num << " Aim: " << aim
        //      << " Target: " << target << " Sum: " << sum
        //      << " Pre: " << pre << endl;

        if (idx >= num.size()) {
            if (sum == target) {
               // cout << "Num: " << num << " Aim: " << aim
               //   << " Target: " << target << " Sum: " << sum
               //   << " Pre: " << pre << endl;
                if (existed[aim] == false) {
                    existed[aim] = true;
                    ans.push_back(aim);
                }
            }
            return;
        }

        for (int i = idx; i < num.length(); i++) {
            if (i > idx && num[idx] == '0') break;
            string sub = num.substr(idx, i - idx + 1);
            long numVal = stol(sub);
            if (idx == 0) {
                // init
                DFS(ans, num, i + 1, target, numVal, numVal, sub);
            }
            else {
                for (int j = 0; j < 3; j++) {
                    switch(j) {
                        case 0: {
                            string add = "+" + sub;
                            DFS(ans, num, i + 1, target, sum + numVal, numVal, aim + add);
                            break;
                        }
                        case 1: {
                            string minus = "-" + sub;
                            DFS(ans, num, i + 1, target, sum - numVal, -numVal, aim + minus);
                            break;
                        }
                        case 2: {
                            string multi = "*" + sub;
                            int multiVal = pre * numVal;
                            DFS(ans, num, i + 1, target, sum - pre + multiVal, multiVal, aim + multi);
                            break;
                        }
                    }
                }
            }
        }
    }
};
