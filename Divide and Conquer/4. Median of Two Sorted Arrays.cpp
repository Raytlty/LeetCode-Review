class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int length1 = nums1.size(), length2 = nums2.size();
        int median;
        if ((length1 + length2) % 2) {
            median = (length1 + length2) / 2 + 1;
            return binarySearch(nums1, 0, length1 - 1, nums2, 0, length2 - 1, median) * 1.0;
        }
        else {
            median = (length1 + length2) / 2;
            return 0.5 * binarySearch(nums1, 0, length1 - 1, nums2, 0, length2 - 1, median)
                 + 0.5 * binarySearch(nums1, 0, length1 - 1, nums2, 0, length2 - 1, median + 1);
        }
    }
    
    double binarySearch(vector<int> &nums1, int s1, int e1,
                        vector<int> &nums2, int s2, int e2,
                        unsigned median) {
        // cout << s1 << " " << e1 << " " << s2 << " " << e2 << endl;
        if (nums1.size() > nums2.size()) {
            return binarySearch(nums2, s2, e2, nums1, s1, e1, median);
        }
        if (s1 > e1) {
            return 1.0 * nums2[s2 + median - 1];
        }
        if (s2 > e2) {
            return 1.0 * nums1[s1 + median - 1];
        }
        int m1 = (s1 + e1) / 2, m2 = (s2 + e2) / 2;
        int half = (m1 - s1 + 1) + (m2 - s2 + 1);
        if (nums1[m1] < nums2[m2]) {
            if (half > median) {
                return binarySearch(nums1, s1, e1, nums2, s2, m2 - 1, median);
            }
            else {
                return binarySearch(nums1, m1 + 1, e1, nums2, s2, e2, median - m1 + s1 - 1);
            }
        }
        else {
            if (half > median) {
                return binarySearch(nums1, s1, m1 - 1, nums2, s2, e2, median);
            }
            else {
                return binarySearch(nums1, s1, e1, nums2, m2 + 1, e2, median - m2 + s2 - 1);
            }
        }
    }
};