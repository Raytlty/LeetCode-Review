class Solution {
    public:
        int countNodes(TreeNode* root) {
            if (!root) return 0;
            int height = 1, leaves = 0;
            TreeNode *p = root, *q;
            bool early = false;
            while (p->left) {
                p = p->left;
                height++;
            }
            if (height == 1) return 1;
            if (height == 2) {
                if (root->right) return 3;
                else return 2;
            }
            for(int level = height - 2; level >=0; --level) {
                p = root->left; q = p;
                for(int i = 0; i < level; ++i) {
                    q = p;
                    p = p->right;
                }
                if (p == NULL) {
                    if (q && q->left) {
                        leaves++;
                        early = true;
                        break;
                    }
                    root = root->left;
                }
                else {
                    root = root->right;
                    leaves += (1 << level);
                }
            }
            return (1 << (height - 1)) + leaves + (early ? 0: root ? 1 : 0) - 1;
        }
};