/*

Refine Tried Tree solution

*/
class Solution {
public:
    unordered_set<string> m_collects;
    vector<string> findAllConcatenatedWordsInADict(vector<string>& words) {
        // return DFS(words);
        return DP(words);
    }
   
    // unorder_set and DFS
    vector<string> DFS(vector<string>& words) {
        vector<string> results;
        sort(words.begin(), words.end(), [&](const string &a, const string &b) -> bool { return a.size() < b.size();});
        for (string word: words) {
            string _word = word + "#";
            if (search(_word)) {
                if (_word == "#") continue;
                results.push_back(word);
            }
            else {
                m_collects.insert(word);
            }
        }
        return results;
    }
    
    bool search(string &word) {
        if (word == "#") return true;
        for (unsigned i = 1; i <= word.size(); i++) {
            string s = word.substr(0, i);
            if (m_collects.find(s) != m_collects.end()) {
                string next = word.substr(i);
                if (search(next)) return true;
            }
        }
        return false;
    }
    
    // Dynamic Programm
    vector<string> DP(vector<string>& words) {
        vector<string> results;
        sort(words.begin(), words.end(), [&](const string &a, const string &b) -> bool { return a.size() < b.size();});
        for(string word: words) {
            if (dp(word)) {
                results.push_back(word);
            }
            else m_collects.insert(word);
        }
        return results;
    }
    
    bool dp(string &word) {
        if (word.empty()) return false;
        vector<bool> used(word.size() + 1, false);
        // bool *used = new bool[word.size() + 1];
        // memset(used, false, sizeof(used));
        used[0] = true;
        for (int i = 1; i <= word.size(); i++) {
            for (int j = i - 1; j >= 0; j--) {
                string s = word.substr(j, i - j); // from back to front, increasing search will save much more time
                if (used[j] && m_collects.find(s) != m_collects.end()) {
                    used[i] = true;
                    break;
                }
            }
        }
        bool ans = used[word.size()];
        // delete []used;
        return ans;
    }
};