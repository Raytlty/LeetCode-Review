// 1409MS
class Solution {
public:
    
    int maxLength;
    
    vector<string> removeInvalidParentheses(string s) {
        vector<string> results;
        if (s.empty() || isValid(s)) {
            results.push_back(s);
            return results;
        }
        map<string, bool> used;
        int length = static_cast<int>(s.size());
        maxLength = length;
        for (unsigned i = 0; i < maxLength; i++) {
            if (s[i] == '(' || s[i] == ')') {
                string sub = subString(i, s);
                DFS(sub, 1, i, length, results, used);
            }
        }
        vector<string> ans;
        for (string x: results) {
            if (static_cast<int>(x.size()) == maxLength - length) ans.push_back(x);
        }
        return ans;
    }
    
    string subString(int idx, string s) {
        string t = "";
        for (unsigned i = 0; i < s.size(); i++) {
            if (idx == i) continue;
            t += s[i];
        }
        return t;
    }
    
    void DFS(string s, int step, int idx, int &depth, vector<string> &results, map<string, bool> &used) {
        if (depth != maxLength && step > depth) return;
        if (used[s] == true) return;
        if (isValid(s) && step <= depth) {
            used[s] = true;
            depth = step;
            results.push_back(s);
            return;
        }
        
        for (unsigned i = idx; i < (int)s.size(); i++) {
            if (s[i] == ')' || s[i] == '(') {
                string sub = subString(i, s);
                DFS(sub, step + 1, i, depth, results, used);
            }
        }
    }
    
    bool isValid(string s) {
        stack<char> inputs;
        for (unsigned i = 0; i < s.size(); i++) {
            if (s[i] == '(') {
                inputs.push('(');
            }
            else if (s[i] == ')') {
                if (inputs.size() == 0) return false;
                inputs.pop();
            }
        }
        if (inputs.size() > 0) return false;
        return true;
    }
};