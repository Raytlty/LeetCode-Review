#include <iostream>
using namespace std;


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
// class Solution {
// public:
//     TreeNode *m_miniumn = new TreeNode(numeric_limits<int>::min());
//     TreeNode *m_root;
    
//     void recoverTree(TreeNode* root) {
//         TreeNode *pre = m_miniumn, *cur = root;
//         m_root = root;
//         inOrder(cur, pre);
//         root = m_root;
//     }
    
//     void swap(TreeNode *&a, TreeNode *&b) {
//         int t = a->val; a->val = b->val; b->val = t;
//     }
    
//     void inOrder(TreeNode *cur, TreeNode *&pre) {
//         if (!cur) {
//             return;
//         }
//         inOrder(cur->left, pre);
//         // cout << pre->val << " " << cur->val << endl;
//         if (pre->val <= cur->val) {
//             // cout << "SWAP" << endl;
//             pre = cur;
//         }
//         else {
//             // cout << "FOUND" << endl;
//             swap(pre, cur);
//             cur = m_root;
//             pre = m_miniumn;
//             inOrder(cur, pre);
//             return;
//         }
//         inOrder(cur->right, pre);
//     }
// }; // 这是解决多个节点数据位置不对的情况, 本题只有两个数据位置不对

class Solution {
public:
    
    TreeNode *a, *b;
    
    void recoverTree(TreeNode* root) {
        TreeNode *pre = new TreeNode(numeric_limits<int>::min()), *cur = root;
        inOrder(cur, pre);
        swap(a, b);
    }
    
    void swap(TreeNode *&a, TreeNode *&b) {
        int t = a->val; a->val = b->val; b->val = t;
    }
    
    void inOrder(TreeNode *cur, TreeNode *&pre) {
        if (!cur) {
            return;
        }
        inOrder(cur->left, pre);
        
        if (pre->val > cur->val) {
            if (a == NULL) a = pre;
            b = cur; // 避免[2, 3, 1]这种情况, 只需要交换左右子树的值
        }
        pre = cur;
        inOrder(cur->right, pre);
    }
};
