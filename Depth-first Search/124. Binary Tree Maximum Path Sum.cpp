/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxPathSum(TreeNode* root) {
        int minium = numeric_limits<int>::min();
        int maxium = maxRootSum(root, minium);
        return minium;
    }
    
    int maxRootSum(TreeNode *root, int &minium) {
        if (!root) return 0;
        int left = max(maxRootSum(root->left, minium), 0);
        int right = max(maxRootSum(root->right, minium), 0);
        
        minium = max(minium, root->val + left  + right);
        return max(left, right) + root->val;
    }
};