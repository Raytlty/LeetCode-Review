class Solution {
public:
    
    char operation[4] = {'+', '-', '*', '/'};
    
    bool judgePoint24(vector<int>& nums) {
        vector<double> ans;
        for (int x: nums) {
            ans.push_back(x * 1.0);
        }
        if (hasPoint24(ans)) return true;
        return false;
    }
    
    double calculate(double first, double second, char op) {
        switch(op) {
            case '+': return first + second;
            case '-': return first - second;
            case '*': return first * second;
            case '/': return first / second;
        }
    }
        
    bool hasPoint24(vector<double> nums) {
        if (nums.size() == 1) {
            double value = nums[0];
            if (abs(value - 24.0) < 0.00001) return true;
        }
        for (unsigned i = 0; i < nums.size(); i++) {
           for (unsigned j = 0; j < nums.size(); j++) {
               if (i != j) {
                   vector<double> rest;
                   for (unsigned k = 0; k < nums.size(); k++) if (k != i && k != j) rest.push_back(1.0 * nums[k]);
                   for (int op = 0; op < 4; op ++) {
                       if (op == 3 && nums[j] - 0.0 < 0.00001) continue;
                       double value = calculate(1.0 * nums[i], 1.0 * nums[j], operation[op]);
                       rest.push_back(value);
                       if (hasPoint24(rest)) return true;
                       rest.pop_back();
                   }
               }
           }
        }
        return false;
    }
};