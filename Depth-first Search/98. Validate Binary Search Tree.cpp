/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isValidBST(TreeNode* root) {
        vector<int> inorder;
        inOrderSearch(root, inorder);
        for (unsigned i = 1; i < inorder.size(); i++) {
            if (inorder[i] <= inorder[i - 1]) return false;
        }
        return true;
    }

    void inOrderSearch(TreeNode *root, vector<int> &inorder) {
        if (!root) return;
        inOrderSearch(root->left, inorder);
        inorder.push_back(root->val);
        inOrderSearch(root->right, inorder);
    }
};