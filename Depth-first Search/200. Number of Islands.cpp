class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
        if (grid.empty()) return 0;
        int rows = grid.size(), columns = grid[0].size();
        vector<vector<bool>> used(rows, vector<bool>(columns, false));
        int islands = 0;
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if (grid[i][j] == '1' && !used[i][j]) {
                    islands++;
                    DFS(i, j, grid, used);
                } 
            }
        }
        return islands;
    }
    
    void DFS(int x, int y, vector<vector<char>> &grid, vector<vector<bool>> &used) {
        used[x][y] = true;
        if (x + 1 < grid.size() && grid[x + 1][y] == '1' && !used[x + 1][y])
            DFS(x + 1, y, grid, used);
        if (y + 1 < grid[0].size() && grid[x][y + 1] == '1' && !used[x][y + 1])
            DFS(x, y + 1, grid, used);
        if (x - 1 >= 0 && grid[x - 1][y] == '1' && !used[x - 1][y])
            DFS(x - 1, y, grid, used);
        if (y - 1 >= 0 && grid[x][y - 1] == '1' && !used[x][y - 1])
            DFS(x, y - 1, grid, used);
    } 
};