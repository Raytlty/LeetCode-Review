//NATIVE DP 26ms
class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int S) {
        int target = 0;
        for (int x: nums) target += x;
        if (target < S) return 0;
        vector<vector<int>> dp(nums.size(), vector<int>(target * 2 + 1, 0));
        for (int i = 0; i < nums.size(); i++) {
            for (int j = 0; j <= 2 * target; j++) {
                if (i == 0) {
                    if (target + j - nums[i] >= 0 && target + j - nums[i] <= 2 * target)
                        dp[i][target + j - nums[i]]++;
                    if (target + j + nums[i] >= 0 && target + j + nums[i] <= 2 * target)
                        dp[i][target + j + nums[i]]++;
                    break;
                }
                else {
                    if (j - nums[i] >= 0 && j - nums[i] <= 2 * target)
                        dp[i][j] = dp[i][j] + dp[i - 1][j - nums[i]];
                    if (j + nums[i] >= 0 && j + nums[i] <= 2 * target)
                        dp[i][j] = dp[i][j] + dp[i - 1][j + nums[i]];
                }
            }
        }
        return dp[nums.size() - 1][S + target];
    }
};

// IMPROVE DP 8MS
class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int S) {
       /*
            S(Positive) - S(Negative) = Target
            S(Positive) - S(Negative) + S(Positive) + S(Negative) = Target + S(Positive) + S(Negative)
            2 * S(Positive) = Target + SUM{nums};
            S(Positive) = (Target + SUM{nums}) / 2
       */
        int total = 0;
        for (int x: nums) total += x;
        if (total < S || (total + S) % 2) return 0;
        vector<int> dp((total + S) / 2 + 1, 0);
        dp[0] = 1;
        for (unsigned i = 0; i < nums.size(); i++) {
            for (int j = (total + S) / 2; j >= nums[i]; j--) {
                dp[j] += dp[j - nums[i]];
            }
        }
        return dp[(total + S) / 2];
    }
};
