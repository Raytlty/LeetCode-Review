
//BFS
class Solution {
private:
    int directed[8][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, -1}, {1, 1}, {-1, -1}, {-1, 1}};
public:
    
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        vector<vector<char>> results;
        if (board.empty()) return results;
        vector<vector<bool>> used(board.size(), vector<bool>(board[0].size(), false));
        int rows = board.size(), columns = board[0].size();
        queue<pair<int, int>> que;
        que.push(make_pair(click[0], click[1]));
        used[click[0]][click[1]] = true;
        while (!que.empty()) {
            auto s = que.front(); que.pop();
            // check if the first one is Mine
            char &ch = board[s.first][s.second];
            if (ch == 'M') {
                ch = 'X';
                continue;
            }
            // check if there are Mine around
            int count = 0;
            for (unsigned i = 0; i < 8; i++) {
                int x = s.first + directed[i][0];
                int y = s.second + directed[i][1];
                if (x >= 0 && x <rows
                    && y >= 0 && y < columns 
                    && (board[x][y] == 'M' || board[x][y] == 'X'))  {
                    count++;
                }
            }
            if (count > 0) {
                ch = count + '0';
                continue;
            }
            // check if the four around can into queue
            ch = 'B';
            used[s.first][s.second] = true;
            for (int i = 0; i < 8; i++) {
                int x = s.first + directed[i][0];
                int y = s.second + directed[i][1];
                if (x >= 0 && x < rows
                    && y >= 0 && y < columns
                    && board[x][y] == 'E'
                    && !used[x][y]) {
                    used[x][y] = true;
                    que.push(make_pair(x, y));
                }
            } 
        }
        return board;
    }
};

//DFS
// class Solution {
// private:
//     int directed[8][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, -1}, {1, 1}, {-1, -1}, {-1, 1}};
// public:
    
//     vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
//         vector<vector<char>> results;
//         if (board.empty()) return results;
//         vector<vector<bool>> used(board.size(), vector<bool>(board[0].size(), false));
//         int rows = board.size(), columns = board[0].size();
//         queue<pair<int, int>> que;
//         que.push(make_pair(click[0], click[1]));
//         used[click[0]][click[1]] = true;
//         while (!que.empty()) {
//             auto s = que.front(); que.pop();
//             // check if the first one is Mine
//             char &ch = board[s.first][s.second];
//             if (ch == 'M') {
//                 ch = 'X';
//                 continue;
//             }
//             // check if there are Mine around
//             int count = 0;
//             for (unsigned i = 0; i < 8; i++) {
//                 int x = s.first + directed[i][0];
//                 int y = s.second + directed[i][1];
//                 if (x >= 0 && x <rows
//                     && y >= 0 && y < columns 
//                     && (board[x][y] == 'M' || board[x][y] == 'X'))  {
//                     count++;
//                 }
//             }
//             if (count > 0) {
//                 ch = count + '0';
//                 continue;
//             }
//             // check if the four around can into queue
//             ch = 'B';
//             used[s.first][s.second] = true;
//             for (int i = 0; i < 8; i++) {
//                 int x = s.first + directed[i][0];
//                 int y = s.second + directed[i][1];
//                 if (x >= 0 && x < rows
//                     && y >= 0 && y < columns
//                     && board[x][y] == 'E'
//                     && !used[x][y]) {
//                     used[x][y] = true;
//                     que.push(make_pair(x, y));
//                 }
//             } 
//         }
//         return board;
//     }
// };

class Solution {
private:
    int directed[8][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, -1}, {1, 1}, {-1, -1}, {-1, 1}};
public:
    
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        vector<vector<char>> results;
        if (board.empty()) return results;
        vector<vector<bool>> used(board.size(), vector<bool>(board[0].size(), false));
        if (board[click[0]][click[1]] == 'M') {
            board[click[0]][click[1]] = 'X';
            return board;
        }
        DFS(board, used, make_pair(click[0], click[1]));
        return board;
    }
    
    bool inboard(vector<vector<char>> &board, pair<int, int> idx) {
        return idx.first >= 0 && idx.first < board.size()
               && idx.second >= 0 && idx.second < board[0].size();
    }

    void DFS(vector<vector<char>> &board, vector<vector<bool>> &used, pair<int, int> idx) {
        if (!inboard(board, idx) || used[idx.first][idx.second]) return;
        int count = 0;
        for (unsigned i = 0; i < 8; i++) {
            int x = directed[i][0] + idx.first, y = directed[i][1] + idx.second;
            auto p = make_pair(x, y);
            if (inboard(board, p) && (board[x][y] == 'M' || board[x][y] == 'X')) count++;
        }
        if (count > 0) {
            board[idx.first][idx.second] = count + '0';
            used[idx.first][idx.second] = true;
            return;
        }
        board[idx.first][idx.second] = 'B';
        used[idx.first][idx.second] = true;
        for (unsigned i = 0; i < 8; i++) {
            int x = directed[i][0] + idx.first, y = directed[i][1] + idx.second;
            auto p = make_pair(x, y);
            if (inboard(board, p) && !used[x][y] && board[x][y] == 'E') {
                DFS(board, used, p);
            }
        }
    }
};