class Solution {
public:
    int shoppingOffers(vector<int>& price, vector<vector<int>>& special, vector<int>& needs) {
        int miniumn = numeric_limits<int>::max();
        vector<int> bucket(needs.size(), 0);
        DFS(price, special, needs, miniumn, 0, bucket);
        return miniumn;
    }
    
    bool fillin(vector<int> &need, vector<int> &bucket) {
        for (unsigned i = 0; i < need.size(); i++) {
            if (need[i] != bucket[i]) return false;
        }
        return true;
    }

    void DFS(vector<int>& price, vector<vector<int>>& special,
             vector<int>& needs, int &miniumn, int sum, vector<int> bucket) {
        if (fillin(needs, bucket)) {
            miniumn = min(miniumn, sum);
            return;
        }
        for (vector<int> sp: special) {
            bool is_break = false;
            for (unsigned i = 0; i < sp.size() - 1; i++) {
                if (sp[i] + bucket[i] > needs[i]) {
                    is_break = true;
                    break;
                }
            }
            if (!is_break) {
                vector<int> temp(bucket.begin(), bucket.end());
                for (unsigned i = 0; i < sp.size() - 1; i++) {
                    temp[i] = bucket[i] + sp[i];
                }
                DFS(price, special, needs, miniumn, sum + sp[sp.size() - 1], temp);
            }
        }
        int _sum = 0;
        for (unsigned i = 0; i < needs.size(); i++) {
            _sum += (needs[i] - bucket[i]) * price[i];
        }
        DFS(price, special, needs, miniumn, sum + _sum, needs);
    }
};