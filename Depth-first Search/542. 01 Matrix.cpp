// class Solution {
//     public: 
//     void printVec(vector<vector<bool>> &used) {
//         for (vector<bool> x: used) {
//             for (bool y: x) {
//                 cout << y << " ";
//             }
//             cout << endl;
//         }
//     }
    
//     vector<vector<int>> updateMatrix(vector<vector<int>> &matrix) {
//         vector<vector<int>> results;
//         if (matrix.empty()) return results;
//         int rows = matrix.size(), columns = matrix[0].size();
//         vector<vector<bool>> used(rows, vector<bool>(columns, false));
//         for (unsigned i = 0; i < rows; i++) {
//             vector<int> temp;
//             for (unsigned j = 0; j < columns; j++) {
//                 if (!matrix[i][j]) temp.push_back(matrix[i][j]);
//                 else {
//                     used[i][j] = true;
//                     int crosses = DFS(matrix, used, i, j, rows, columns);
//                     temp.push_back(crosses);
//                     used[i][j] = false;
//                 }
//             }
//             results.push_back(temp);
//         }
//         return results;
//     }
    
//     int DFS(vector<vector<int>> &matrix, vector<vector<bool>> &used, int x, int y, int rows, int columns) {
//         if (!matrix[x][y]) return 0;
//         int maxium = numeric_limits<int>::max() >> 1;
//         int left = maxium;
//         int right = maxium;
//         int up = maxium;
//         int down = maxium;
//         if (x - 1 >= 0 && !used[x - 1][y]) {
//             used[x - 1][y] = true;
//             left = DFS(matrix, used, x - 1, y, rows, columns) + 1;
//             used[x - 1][y] = false;
//         }
//         if (x + 1 < rows && !used[x + 1][y]) {
//             used[x + 1][y] = true;
//             right = DFS(matrix, used, x + 1, y, rows, columns) + 1;
//             used[x + 1][y] = false;
//         }
//         if (y - 1 >= 0 && !used[x][y - 1]) {
//             used[x][y - 1] = true;
//             up = DFS(matrix, used, x, y - 1, rows, columns) + 1;
//             used[x][y - 1] = false;
//         }
//         if (y + 1 < columns && !used[x][y + 1]) {
//             used[x][y + 1] = true;
//             down = DFS(matrix, used, x, y + 1, rows, columns) + 1;
//             used[x][y + 1] = false;
//         }
//         return min(min(left, right), min(up, down));
//     }
// }; // TLE, maybe use memorized refine


class Solution {
public:
    vector<pair<int,int> > dir = {{1,0},{-1,0},{0,1},{0,-1}};
    vector<vector<int> > updateMatrix(vector<vector<int> >& matrix) {
        if(matrix.empty()) return matrix;
        int m = matrix.size();
        int n = matrix[0].size();
        queue<pair<int,int>> zeros;
        for(int i = 0; i < m; ++i){
            for(int j = 0; j < n; ++j){
                if(matrix[i][j] == 0){
                    zeros.push({i,j});
                }else{
                    matrix[i][j] = numeric_limits<int>::max();
                }
            }
        }
        while(!zeros.empty()){
            auto xy = zeros.front();
            zeros.pop();
            int i = xy.first, j = xy.second;
            for(auto d : dir){
                int ii = i + d.first, jj = j + d.second;
                if(ii < m && ii >= 0 && jj < n && jj >= 0){
                    if(matrix[ii][jj] >= matrix[i][j] + 1){
                        matrix[ii][jj] = matrix[i][j] + 1;
                        zeros.push({ii,jj});
                    }
                }
            }
        }
        return matrix;
    }
};