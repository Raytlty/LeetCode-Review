// TODO: USE MERGE SORT SOLVE
class Solution {
public:
    vector<vector<int>> findSubsequences(vector<int>& nums) {
        vector<vector<int>> results;
        return results;
    }
    
    vector<vector<int>> mergeSort(vector<int> &nums, vector<int> &copy, int left, int right) {
        if (left < right) {
            int middle = (left + right) / 2;
            vector<vector<int>> leftVec = mergeSort(copy, nums, left, middle);
            vector<vector<int>> rightVec = mergeSort(copy, nums, middle + 1, right);
            vector<vector<int>> curVec;
            
            int ll = left, rr = middle + 1;
            int cur = left;
            while (ll <= middle && rr <= right) {
                while(ll <= middle && nums[ll] <= nums[rr]) {
                    copy[cur++] = nums[ll++];
                }
                while (rr <= right && nums[rr] < nums[ll]) {
                    copy[cur++] = nums[rr++];
                }
            }
        }
    }
};

