/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    void connect(TreeLinkNode *root) {
        if (!root) return;
        link(root->left, root->right);
    }
    
    void link(TreeLinkNode *left, TreeLinkNode *right) {
        if (!left||!right) {
            return;
        }
        left->next = right;
        if ((!left->left&&!left->right)||(!right->left&&!right->right)) return;
        link(left->left, left->right);
        link(left->right, right->left);
        link(right->left, right->right);
    }
};