/**
 * Definition for undirected graph.
 * struct UndirectedGraphNode {
 *     int label;
 *     vector<UndirectedGraphNode *> neighbors;
 *     UndirectedGraphNode(int x) : label(x) {};
 * };
 */
class Solution {
public:
    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
        if (!node) return NULL;
        unordered_map<int, UndirectedGraphNode*> nodes;
        return DFS(node, nodes);
    }
    
    UndirectedGraphNode *DFS(UndirectedGraphNode *root,
                             unordered_map<int, UndirectedGraphNode *> &nodes) {
        if (nodes[root->label]) return nodes[root->label];
        UndirectedGraphNode *node = new UndirectedGraphNode(root->label);
        nodes[node->label] = node;
        for (UndirectedGraphNode * neighbor: root->neighbors) {
            node->neighbors.push_back(DFS(neighbor, nodes));
        }
        return node;
    }
};