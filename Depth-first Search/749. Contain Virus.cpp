// WRONG ANSWER
class Solution {
public:
    int direct[4][2] = {{1, 0}, {0, -1}, {0, 1}, {-1, 0},};
    
    int containVirus(vector<vector<int>>& grid) {
        int rows = grid.size(), columns = grid[0].size();
        vector<vector<bool>> used(rows, vector<bool>(columns, false));
        vector<pair<pair<int, int>, int>> updated;
        vector<pair<int, int>> vec;
        queue<pair<int, int>> que;
        int maxInject = 0;
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if (grid[i][j] == 1 && !used[i][j]) {
                    que.push(make_pair(i, j));
                    used[i][j] = true;
                    vector<pair<pair<int, int>, int>> temp;
                    int inject = 0;
                    while (!que.empty()) {
                        auto p = que.front(); que.pop();
                        vec.push_back(p);
                        int x = p.first, y = p.second;
                        for (int d = 0; d < 4; d++) {
                            int dx = x + direct[d][0], dy = y + direct[d][1];
                            if (dx >= 0 && dx < rows && dy >= 0 && dy < columns) {
                                if (grid[dx][dy] == 1 && !used[dx][dy]) {
                                    used[dx][dy] = true;
                                    que.push(make_pair(dx, dy));
                                }
                                else if (grid[dx][dy] != 1 && grid[dx][dy] >= 0) {
                                    int defend = 1 << (d + 1);
                                    if (!(grid[dx][dy] & defend)) {
                                        inject++;
                                        temp.push_back(make_pair(make_pair(dx, dy), d + 1));
                                    }
                                }
                            }
                        }
                    }
                    if (maxInject < inject) {
                        updated = temp;
                        maxInject = inject;
                    }
                }
            }
        }
                
        if (maxInject <= 0) return 0;
        
        // need to update grid by updated
        for (vector<pair<pair<int, int>, int>>::iterator iter = updated.begin();
             iter != updated.end(); iter++) {
            int x = (iter->first).first, y = (iter->first).second;
            // cout << x << " " << y << endl;
            grid[x][y] += (1 << iter->second);
        }
        
        for (vector<pair<int, int>>::iterator iter = vec.begin();
             iter != vec.end(); iter++) {
            int x = iter->first, y = iter->second;
            bool guard = true;
            for (int d = 0; d < 4; d++) {
                int dx = direct[d][0] + x, dy = direct[d][1] + y;
                if (dx >= 0 && dx < rows && dy >= 0 && dy < columns) {
                    int defend = 1 << (d + 1);
                    if (grid[dx][dy] != 1 && !(grid[dx][dy] & defend)) {
                        grid[dx][dy] = 1; guard = false;
                    }
                }
            }
            if (guard && grid[x][y] != 1) grid[x][y] = -1;
        }    
        
        return maxInject + containVirus(grid);
    }
};

// Solution 2, Still WRONG
class Solution {
public:
    int direct[4][2] = {{1, 0}, {0, -1}, {0, 1}, {-1, 0},};
    bool first = true;
    
    int allChange(vector<vector<int>> &grid) {
        int total = 0;
        for (unsigned i = 0; i < grid.size(); i++)
            for (unsigned j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] == 1) return 0;
                if (grid[i][j] >= 0) total++;
            }
        return total;
    }
    
    void output(vector<vector<int>> &m) {
        for (unsigned i = 0; i < m.size(); i++) {
            for (unsigned j = 0; j < m[0].size(); j++) {
                cout << m[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
    
    void buildWallStopVirus(vector<vector<int>> grid, int maxInject, int preInject,
                            int &minInject, int &maxProtect) {
        int total = allChange(grid);
        if (total){
            cout << maxInject << " " << total << endl;
            output(grid);
            if (total >= maxProtect) {
                if ((total == maxProtect && minInject > maxInject)
                    || total > maxProtect)
                    minInject = maxInject;
                maxProtect = total;
            }
            return;
        }
        int rows = grid.size(), columns = grid[0].size();
        vector<vector<pair<int, int>>> groups;
        vector<vector<bool>> used(rows, vector<bool>(columns, false));
        // 分组
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if (!used[i][j] && grid[i][j] == 1) {
                    vector<pair<int, int>> vec;
                    queue<pair<int, int>> que;
                    auto p = make_pair(i, j);
                    used[i][j] = true;
                    que.push(p);
                    vec.push_back(p);
                    while (!que.empty()) {
                        p = que.front(); que.pop();
                        int x = p.first, y = p.second;
                        for (int d = 0; d < 4; d++) {
                            int dx = x + direct[d][0], dy = y + direct[d][1];
                            if (dx >= 0 && dx < rows && dy >= 0 && dy < columns
                                && grid[dx][dy] == 1 && !used[dx][dy]) {
                                p = make_pair(dx, dy);
                                used[dx][dy] = true;
                                que.push(p);
                                vec.push_back(p);
                            }
                        }
                    }
                    groups.push_back(vec);
                }
            }
        }
        //分治
        for (unsigned i = 0; i < groups.size(); i++) {
            int inject = 0;
            auto matrix = getNewGrid(grid, groups[i], groups, i, inject);
            buildWallStopVirus(matrix, maxInject + inject, inject, minInject, maxProtect);
        }
    }
    
    vector<vector<int>> getNewGrid(vector<vector<int>> grid, vector<pair<int, int>> &group, 
                                   vector<vector<pair<int, int>>> &groups, int groupIdx, int &inject) {
        int rows = grid.size(), columns = grid[0].size();
        // build wall
        for (auto pair: group) {
            int x = pair.first, y = pair.second;
            grid[x][y] = -1;
            for (int d = 0; d < 4; d++) {
                int dx = x + direct[d][0], dy = y + direct[d][1];
                if (dx >= 0 && dx < rows && dy >= 0 && dy < columns
                    && grid[dx][dy] != 1 && grid[dx][dy] >= 0){
                    int wall = 1 << (4 - d);
                    if (!(grid[dx][dy] & wall)) {
                        inject++;
                        grid[dx][dy] += wall;
                    }
                }
            } 
        }
        // next day, virus expand
        for (unsigned i = 0; i < groups.size(); i++) {
            if (i != groupIdx) {
                for (auto pair: groups[i]) {
                    for (int d = 0; d < 4; d++) {
                        int dx = pair.first + direct[d][0];
                        int dy = pair.second + direct[d][1];
                        if (dx >= 0 && dx < rows && dy >= 0 && dy < columns
                            && grid[dx][dy] != 1) {
                            int wall = 1 << (4 - d);
                            if (!(grid[dx][dy] & wall)) {
                                grid[dx][dy] = 1;
                            }
                        }
                    }
                }
            }
        }
        
//         // virus expand II
//         queue<pair<int, int>> que;
//         for (unsigned i = 0; i < groups.size(); i++) {
//             if (i != groupIdx) {
//                 for (auto pair: groups[i]) {
//                     que.push(pair);
//                 }
//             }
//         }
        
//         while (!que.empty()) {
//             auto pair = que.front(); que.pop();
//             int x = pair.first, y = pair.second;
//             for (int d = 0; d < 4; d++) {
//                 int dx = x + direct[d][0];
//                 int dy = y + direct[d][1];
//                 if (dx >= 0 && dx < rows && dy >= 0 && dy < columns
//                     && grid[dx][dy] == -1 && grid[x][y] == 1) {
//                     grid[dx][dy] = 1;
//                     que.push(make_pair(dx, dy));
//                 }
//             }
//         }
        
        return grid;
    }
    
    int containVirus(vector<vector<int>>& grid) {
        int miniumn = numeric_limits<int>::max();
        int maxiumn = 0;
        buildWallStopVirus(grid, 0, -1, miniumn, maxiumn);
        return miniumn;
    }
};

//ACCEPT
class Solution {
public:
    int direct[4][2] = {{1, 0}, {-1, -0}, {0, 1}, {0, -1},};
  
    void output(vector<vector<int>> &m) {
        for (unsigned i = 0; i < m.size(); i++) {
            for (unsigned j = 0; j < m[0].size(); j++) {
                cout << m[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
    
    map<pair<int, int>, int> calInject(vector<vector<int>> &grid, vector<pair<int, int>> &group) {
        int rows = grid.size(), columns = grid[0].size();
        map<pair<int, int>, int> inject; 
        for (auto pair: group) {
            int x = pair.first, y = pair.second;
            for (int d = 0; d < 4; d++) {
                int dx = x + direct[d][0], dy = y + direct[d][1];
                if (dx >= 0 && dx < rows && dy >= 0 && dy < columns 
                    && grid[dx][dy] == 0 && grid[x][y] == 1) {
                    inject[make_pair(dx, dy)]++;
                }
            }
        }
        return inject;
    }
    
    int calVirus(vector<pair<int, int>> &group) {
        set<pair<int, int>> s;
        for (auto p: group) s.insert(p);
        return (int)s.size();
    }
    
    int buildWallStopVirus(vector<vector<int>> &grid) {
        int rows = grid.size(), columns = grid[0].size();
        vector<vector<pair<int, int>>> groups;
        vector<vector<bool>> used(rows, vector<bool>(columns, false));
        // 分组
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if (!used[i][j] && grid[i][j] == 1) {
                    vector<pair<int, int>> vec;
                    queue<pair<int, int>> que;
                    auto p = make_pair(i, j);
                    used[i][j] = true;
                    que.push(p);
                    vec.push_back(p);
                    while (!que.empty()) {
                        p = que.front(); que.pop();
                        int x = p.first, y = p.second;
                        for (int d = 0; d < 4; d++) {
                            int dx = x + direct[d][0], dy = y + direct[d][1];
                            if (dx >= 0 && dx < rows && dy >= 0 && dy < columns
                                && grid[dx][dy] == 1 && !used[dx][dy]) {
                                p = make_pair(dx, dy);
                                used[dx][dy] = true;
                                que.push(p);
                                vec.push_back(p);
                            }
                        }
                    }
                    groups.push_back(vec);
                }
            }
        }
        if (groups.empty()) return 0;
        
        // PartI
        // int maxInject = 0;
        // int maxIdx = -1;
        // int maxSize = 0;
        // // 选择影响范围最大的连续区块
        // for (unsigned i = 0; i < groups.size(); i++) {
        //     int virus = calVirus(groups[i]);
        //     if (maxSize < virus) {
        //         maxSize = virus;
        //         cout << maxSize << endl;
        //         int inject = 0;
        //         for (auto pair: groups[i]) {
        //             int x = pair.first, y = pair.second;
        //             for (int d = 0; d < 4; d++) {
        //                 int dx = x + direct[d][0], dy = y + direct[d][1];
        //                 if (dx >= 0 && dx < rows && dy >= 0 && dy < columns 
        //                     && grid[dx][dy] == 0 && grid[x][y] == 1) inject++;
        //             }
        //         }
        //         maxInject = inject;
        //         maxIdx = i;
        //     }
        // }
       
        // PartII
        // 选择影响范围最大的连续区块, 需要注意以块为单位, 每个块最多影响4个相邻位置
        // 相邻位置可能同时被两个块所印象, 但这个位置只能被计算一次
        int maxSize = 0, maxIdx = -1;
        int maxInject = 0;
        for (unsigned i = 0; i< groups.size(); i++) {
            auto m = calInject(grid, groups[i]);
            if (maxSize < (int)m.size()) {
                maxSize = (int)m.size();
                maxIdx = i;
                int inject = 0;
                for(map<pair<int, int>, int>::iterator iter = m.begin();
                    iter != m.end(); iter++) 
                    inject += iter->second;
                maxInject = inject;
            }
        }
        if (maxIdx == -1) return 0;
        
        // 被选中的就置为-1, 不需要再加入计算
        for (auto pair: groups[maxIdx]) {
            int x = pair.first, y = pair.second;
            grid[x][y] = -1;
        }
        
        // 剩下的expand virus
        for (unsigned i = 0; i < groups.size(); i++) {
            if (i == maxIdx) continue;
            for (auto pair: groups[i]) {
                int x = pair.first, y = pair.second;
                
                for (int d = 0; d < 4; d++) {
                    int dx = x + direct[d][0], dy = y + direct[d][1];
                    if (dx >= 0 && dx < rows && dy >= 0 && dy < columns && grid[dx][dy] != -1) {
                        grid[dx][dy] = 1;
                    }
                }
            }
        } 
        // cout << maxInject << endl;
        // output(grid);
        return maxInject + buildWallStopVirus(grid);
    }
    
    int containVirus(vector<vector<int>>& grid) {
        return buildWallStopVirus(grid);
    }
};