/*
// Employee info
class Employee {
public:
    // It's the unique ID of each node.
    // unique id of this employee
    int id;
    // the importance value of this employee
    int importance;
    // the id of direct subordinates
    vector<int> subordinates;
};
*/
class Solution {
public:
    int getImportance(vector<Employee*> employees, int id) {
        int importance = 0;
        unordered_map<int, Employee*> employeeMap;
        queue<Employee*> que;
        for (Employee *& employee: employees) {
            if (employee->id == id) {
                que.push(employee);
            } 
            employeeMap[employee->id] = employee;
        }
        
        while(!que.empty()) {
            Employee* employee = que.front(); que.pop();
            importance += employee->importance;
            for (int id: employee->subordinates) {
                que.push(employeeMap[id]);
            }
        }
        return importance;
    }
};