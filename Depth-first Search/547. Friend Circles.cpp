class Solution {
public:
    
    void initiation(vector<int> &parent) {
        for (unsigned i = 0; i < parent.size(); i++) {
            parent[i] = i;
        }
    }
    
    unsigned find(vector<int> &parent, unsigned x) {
        int fx = x;
        while (parent[fx] != fx) {
            fx = parent[fx];
        }
        while (x != fx) {
            int t = parent[x];
            parent[t] = fx;
            x = t;
        }
        return fx;
    }
    
    int findCircleNum(vector<vector<int>>& M) {
        int circle = 0;
        vector<int> parent(M.size());
        initiation(parent);
        for (unsigned i = 0; i < M.size(); i++) {
            for (unsigned j = i  ; j < M[i].size(); j++) {
                if(i == j) continue;
                if (M[i][j]) {
                    int fx = find(parent, i), fy = find(parent, j);
                    // cout << fx << " " << fy << endl;
                    if (fx != fy) {
                        parent[fy] = fx;
                    }
                }
            }
        }
        unordered_set<unsigned> res;
        for (unsigned i = 0; i < M.size(); i++) res.insert(find(parent, i));
        return static_cast<int>(res.size());
    }
};