class Solution {
private:
    template<typename T>
    struct Heap {
        vector<T> heap;
        int heapLength = -1;
        Heap(vector<T> &lhs) {
            heap = lhs;
            int length = heap.size();
            heapLength = length;
            int half = length / 2 - 1;
            while (half >= 0) {
                adjustHeap(heap, half, length);
                half--;
            }
            length--;
            while (length >= 0) {
                swap(heap[0], heap[length]);
                adjustHeap(heap, 0, length);
                length--;
            }
        }
        
        void swap(T &a, T &b) {
            T t = a; a = b; b = t;
        }
        
        void adjustHeap(vector<T> &lhs, int start, int length) {
            T temp = lhs[start];
            int pos = start;
            while (start < length) {
                pos = start;
                start = start * 2 + 1;
                if (start >= length) break;
                if (start + 1 < length && lhs[start + 1] < lhs[start])
                    start++;
                if (start < length && temp < lhs[start])
                    break;
                lhs[pos] = lhs[start];
            }
            lhs[pos] = temp;
        }
        
        T getMin() {
            return heap.back();
        }
        
        void popBack() {
            heap.pop_back();
        }
        
        bool empty() {
            return heap.size() == 0;
        }
    };
    
public:
    vector<string> findItinerary(vector<pair<string, string>> tickets) {
        vector<string> res;
        map<string, vector<string>> m;
        map<string, Heap<string>> m2;
        for (auto ticket: tickets) {
            m[ticket.first].push_back(ticket.second);
        }
        for (auto it = m.begin(); it != m.end(); it++) {
            m2[it->first] = Heap<string>(it->second);
            m2[it->first].getMin();
        }
        
        // vector<string> vec;
        // vec.push_back("JFK");
        // while (!vec.empty()) {
        //     string from = vec.back();
        //     if (m2[from].empty()) {
        //         res.push_back(from);
        //         vec.pop_back();
        //         m2[vec.back()].popBack();
        //     }
        //     else {
        //         string to = m2[from].getMin();
        //         vec.push_back(to);
        //     }
        // }
        // reverse(begin(res), end(res));
        return res;
    }
    
}; // NOT RIGHT

// 模拟栈 16MS
class Solution {
public:
    vector<string> findItinerary(vector<pair<string, string>> tickets) {
        vector<string> res;
        map<string, vector<string>> m;
        for (auto ticket: tickets) {
            m[ticket.first].push_back(ticket.second);
        }
        for (auto it = m.begin(); it != m.end(); it++) {
            sort(it->second.begin(), it->second.end(), greater<string>());
        }
        vector<string> vec = {"JFK"};
        while (!vec.empty()) {
            string from = vec.back();
            if (m[from].empty()) {
                res.push_back(from);
                vec.pop_back();
            }
            else {
                string to = m[from].back();
                m[from].pop_back();
                vec.push_back(to);
            }
        }
        reverse(begin(res), end(res));
        return res;
    }
};