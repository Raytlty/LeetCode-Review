//TIMEOUT LIMIT ERROR FUCK!!   
class Solution {
public:
    void solve(vector<vector<char>>& board) {
        if (board.empty()) return;
        int dir[][2] = {{0, -1}, {0, 1}, {1, 0}, {-1, 0}};
        int rows = board.size(), columns = board[0].size();
        int size = max(rows, columns);
        queue<pair<int, int>> que;
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if (i == 0 || i == rows - 1 || j == 0 || j == columns - 1) {
                    continue;
                }
                if (board[i][j] == 'O') {
                    bool filled = true;
                    for (int d = 0; d < 4; d++) {
                        int x = i + dir[d][0], y = j + dir[d][1];
                        if (x >= 0 && x < rows && y >= 0 && y < columns) {
                            if ((x == 0 || x == rows - 1 
                                || y == 0 || y == columns - 1)
                               && board[x][y] == 'O') {
                                filled = false;
                                break;
                            }
                        }
                    }
                    if (filled) {
                        board[i][j] = 'B';
                        que.push(make_pair(i * size + j, 0));
                    }
                }
            }
        }
        while (!que.empty()) {
            auto p = que.front(); que.pop();
            bool filled = true;
            int x = p.first / size, y = p.first % size;
            if (board[x][y] == 'X') continue;
            int count = p.second;
            for (int d = 0; d < 4; d++) {
                int dx = dir[d][0] + x, dy = dir[d][1] + y;
                if (dx >= 0 && dx < rows && dy >= 0 && dy < columns) {
                    if (board[dx][dy] == 'O') {
                        board[x][y] = 'O';
                        filled = false;
                        break;
                    }
                    else if (board[dx][dy] == 'B') {
                        que.push(make_pair(p.first, ++count));
                    }
                }
            }
            if (x== 1 && y == 1)
                cout << count << endl;
            if (filled && (count == 0 || count == p.second || count == size)) board[x][y] = 'X';
        }
    }
};

// RUNTIME ERROR
class Solution {
public:
    int dir[4][2] = {{0, -1}, {0, 1}, {1, 0}, {-1, 0}};
    bool debug = false;
    void solve(vector<vector<char>>& board) {
        if (board.empty()) return;
        int rows = board.size(), columns = board[0].size();
        vector<vector<bool>> mark(rows, vector<bool>(columns, false));
        vector<vector<bool>> used(rows, vector<bool>(columns, false));
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if (i == 0 || i == rows - 1 || j == 0 || j == columns - 1) {
                    if (board[i][j] == 'O') mark[i][j] = true;
                    continue;
                }
                if (board[i][j] == 'O') {
                    used.assign(used.size(), false);
                    if (boardcast(i, j, rows, columns, used, mark, board)) {
                        continue;
                    }
                    else board[i][j] = 'X';
                }
            }
        }
    }
    
    bool boardcast(int x, int y, int rows, int columns,
                   vector<vector<bool>> &used, vector<vector<bool>> &mark,
                   vector<vector<char>> &board) {
        cout << x << " " << y << endl; 
        if (x == 0 || x == rows - 1 || y == 0 || y == columns - 1) {
            if (board[x][y] == 'O') {
                mark[x][y] = true;
                return true;
            }
        }
        if (x < 0 || x >= rows || y < 0 || y >= columns) return false;
        if (board[x][y] == 'X') return false;
        if (mark[x][y]) return true;
        used[x][y] = true;
        for (int d = 0; d < 4; d++) {
            int dx = dir[d][0] + x, dy = dir[d][1] + y;
            if (dx >= 0 && dx < rows && dy >= 0 && dy < columns && !used[dx][dy]) {
                if (boardcast(dx, dy, rows, columns, used, mark, board)) {
                    mark[x][y] = true;
                    return true;
                }
            }
        }
        return false;
    }
};

//ACCEPT
class Solution {
public:
    int dir[4][2] = {{0, -1}, {0, 1}, {1, 0}, {-1, 0}};
    bool debug = false;
    void solve(vector<vector<char>>& board) {
        if (board.empty()) return;
        int rows = board.size(), columns = board[0].size();
        int size = max(rows, columns);
        queue<int> que;
        map<int, bool> mp;
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if ((i == 0 || i == rows - 1 || j == 0 || j == columns - 1) && board[i][j] == 'O') {
                    que.push(i * size + j);
                    mp[i * size + j] = true;
                }
            }
        }
        while (!que.empty()) {
            auto p = que.front(); que.pop();
            int x = p / size, y = p % size;
            for (int d = 0; d < 4; d++) {
                int dx = dir[d][0] + x, dy = dir[d][1] + y;
                if (dx >= 0 && dx < rows && dy >= 0 && dy < columns && board[dx][dy] == 'O' && !mp[dx * size + dy]) {
                    que.push(dx * size + dy);
                    mp[dx * size + dy] = true;
                }
            }
        }
        
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                if (board[i][j] == 'O' && !mp[i * size + j]) {
                    board[i][j] = 'X';
                }
            }
        }
    }
};