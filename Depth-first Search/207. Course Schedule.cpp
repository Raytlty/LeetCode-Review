class Solution {
public:
    bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
        vector<vector<bool>> graph(numCourses, vector<bool>(numCourses, false));
        vector<int> visited(numCourses, 0);
        for (auto p: prerequisites) {
            graph[p.second][p.first] = 1;
        }
        for (int i = 0; i < numCourses; i++) {
            if (!visited[i] && !DFS(i, numCourses, visited, graph)) return false;
        }
        return true;
    }
    
    bool DFS(int u, int numCourses, vector<int> &visited, vector<vector<bool>> &graph) {
        visited[u] = -1;
        for (int i = 0; i < numCourses; i++) {
            if (u == i) continue;
            if (graph[u][i]) {
                if (visited[i] < 0) return false;
                if (!visited[i] && !DFS(i, numCourses, visited, graph)) return false;
            }
        }
        visited[u] = 1;
        return true;
    }
};