/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode* root) {
        if (!root) return;
        TreeNode *left = root->left, *right = root->right;
        TreeNode *node = root;
        flatten(right);
        if (left) {
            flatten(left);
            root->left = nullptr;
            while (left) {
                node->right = left;
                node = left;
                left = left->right;
            }
            node->right = right;
        }
    }
};

//PostOrder Algorithm
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode* root) {
        if (!root) return;
        // postorder search
        flatten(root->left);
        flatten(root->right);
        // now return to root;
        if (!root->left) return;
        TreeNode *ptr = root->left;
        while (ptr->right) ptr = ptr->right;
        ptr->right = root->right;
        root->right = root->left;
        root->left = nullptr;
        return;
    }
};
