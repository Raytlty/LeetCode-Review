class Solution {
public:
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int newColor) {
        if (image.empty()) return image;
        vector<vector<bool>> used(image.size(), vector<bool>(image[0].size(), false));
        int original = image[sr][sc];
        DFS(image, used, sr, sc, original, newColor);
        return image;
    }
    
    void DFS(vector<vector<int>> &image, vector<vector<bool>> &used, int sr, int sc, int original, int newColor) {
        // cout << sr << " " << sc << endl;
        if (sr < 0 || sr >= image.size()
            || sc < 0 || sc >= image[0].size()
            || image[sr][sc] != original || used[sr][sc]) {
            return;
        }
        image[sr][sc] = newColor;
        used[sr][sc] = true;
        DFS(image, used, sr - 1, sc, original, newColor);
        DFS(image, used, sr + 1, sc, original, newColor);
        DFS(image, used, sr, sc - 1, original, newColor);
        DFS(image, used, sr, sc + 1, original, newColor);
    }
};