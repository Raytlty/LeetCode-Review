/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> result;
        if (!root) return result;
        searchPath(root, result, "");
        return result;
    }
    
    void searchPath(TreeNode *root, vector<string> &result, string path) {
        string road = path + to_string(root->val);
        if (!root->left && !root->right) {
            result.push_back(road);
            return;
        }
        if (root->left) 
            searchPath(root->left, result, road + "->");
        if (root->right)
            searchPath(root->right, result, road + "->");
    }
};