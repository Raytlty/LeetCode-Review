/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    void connect(TreeLinkNode *root) {
        if (!root) return;
        queue<TreeLinkNode*> que;
        TreeLinkNode *head = root;
        que.push(root);
        while(!que.empty()) {
            vector<TreeLinkNode*> vec;
            vec.clear();
            while(!que.empty()) {
                vec.push_back(que.front());
                que.pop();
            }
            for (int i = 0; i < (int)vec.size(); i++) {
                if (i + 1 >= (int)vec.size()) {
                    vec[i]->next = nullptr;
                }
                else {
                    vec[i]->next = vec[i + 1];
                }
                if (vec[i]->left) que.push(vec[i]->left);
                if (vec[i]->right) que.push(vec[i]->right);
            }
        }
    }
};