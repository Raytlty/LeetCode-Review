class Solution {
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        if (grid.empty()) return 0;
        vector<vector<bool>> used(grid.size(), vector<bool>(grid[0].size(), false));
        int maxium = 0;
        for (unsigned i = 0; i < grid.size(); i++) {
            for (unsigned j = 0; j < grid[i].size(); j++) {
                if (!used[i][j] && grid[i][j]) {
                    maxium = max(maxium, getMaxArea(grid, used, i, j));
                    // cout << i << " " << j << " " << maxium << endl;
                }
            }
        }
        return maxium;
    }
    
    int getMaxArea(vector<vector<int>> &grid, vector<vector<bool>> &used, int s, int e) {
        if (s < 0 || s >= grid.size() || e < 0 || e >= grid[s].size()
           || used[s][e] || !grid[s][e]) {
            return 0;
        }
        used[s][e] = true;
        int up = getMaxArea(grid, used, s + 1, e);
        int down = getMaxArea(grid, used, s - 1, e);
        int left = getMaxArea(grid, used, s, e - 1);
        int right = getMaxArea(grid, used, s, e + 1);
        return grid[s][e] + up + down + left + right;
    }
};