class Solution {
public:
    string decodeString(string s) {
        if (s.empty()) return "";
        int time = 0;
        string res = "";
        for (unsigned i = 0; i < s.size(); i++) {
            if (isdigit(s[i])) {
                time = time * 10 + s[i] - '0';
            }
            else if (s[i] == '[') {
                int matched = 1;
                i++;
                string tmp = "";
                if (s[i] == ']') {
                    multiString(res, tmp, time);
                    time = 0;
                    continue;
                }
                while (i < s.size() && matched) {
                    tmp.push_back(s[i++]);
                    if (s[i] == '[') matched++;
                    if (s[i] == ']') matched--;
                }
                string decode = decodeString(tmp);
                if (!decode.empty()) 
                    multiString(res, decode, time);
                time = 0;
            }
            else {
                res.push_back(s[i]);
            }
        }
        return res;
    }
    
    void multiString(string &res, string &s, int time) {
        string t = s;
        for (int i = 1; i <= time - 1; i++) {
            s += t;
        }
        res += s;
    }
};