/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> pathSum(TreeNode* root, int sum) {
        vector<vector<int>> paths;
        vector<int> path;
        DFS(root, sum, 0, path, paths);
        return paths;
    }
    
    void DFS(TreeNode *root, int sum, int target, vector<int> &path, vector<vector<int>> &paths) {
        if (!root) {
            return;
        }
        path.push_back(root->val);
        target += root->val;
        if (!root->left && !root->right && sum == target) paths.push_back(path);
        DFS(root->left, sum, target, path, paths);
        DFS(root->right, sum, target, path, paths);
        target -= root->val;
        path.pop_back();
    }
};