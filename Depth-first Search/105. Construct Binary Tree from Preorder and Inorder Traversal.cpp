/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        TreeNode * root = getTree(preorder, inorder, 0, 0, (int)inorder.size());
        return root;
    }
    
    TreeNode* getTree(vector<int> &preorder, vector<int> &inorder, int idx, int left, int right) {
        if (left >= right) return nullptr;
        
        // cout << idx << " " << left << " " << right << endl;
        TreeNode *root = new TreeNode(preorder[idx]);
        int middle = -1;
        for (int i = left; i < right; i++) {
            if (preorder[idx] == inorder[i]) {
                middle = i;
                break;
            }
        }
        if (middle >= 0) {
            root->left = getTree(preorder, inorder, idx + 1, left, middle);
            root->right = getTree(preorder, inorder, idx + middle - left + 1, middle + 1, right);
        }
        return root;
    }
};