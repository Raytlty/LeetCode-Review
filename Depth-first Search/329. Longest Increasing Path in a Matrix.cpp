class Solution {
public:
    int longestIncreasingPath(vector<vector<int>>& matrix) {
        if (matrix.empty()) return 0;
        int rows = matrix.size(), columns = matrix[0].size();
        vector<vector<int>> used(rows, vector<int>(columns, 0));
        int maxiumn = 0;
        for (unsigned i = 0; i < rows; i++) {
            for (unsigned j = 0; j < columns; j++) {
                maxiumn = max(maxiumn, DFS(i, j, matrix, used));
            }
        }
        return maxiumn;
    }
    
    int DFS(int i, int j, vector<vector<int>> &matrix, vector<vector<int>> &used) {
        if (used[i][j]) return used[i][j];
        int path = 0;
        if (i + 1 >= 0 && i + 1 < matrix.size() && matrix[i + 1][j] > matrix[i][j])
            path = max(path, DFS(i + 1, j, matrix, used));
        if (j + 1 >= 0 && j + 1 < matrix[0].size() && matrix[i][j + 1] > matrix[i][j])
            path = max(path, DFS(i, j + 1, matrix, used));
        if (i - 1 >= 0 && i - 1 < matrix.size() && matrix[i - 1][j] > matrix[i][j])
            path = max(DFS(i - 1, j, matrix, used), path);
        if (j - 1 >= 0 && j - 1 < matrix[0].size() && matrix[i][j - 1] > matrix[i][j])
            path = max(path, DFS(i, j - 1, matrix, used));
        used[i][j] = ++path;
        return path;
    }
};