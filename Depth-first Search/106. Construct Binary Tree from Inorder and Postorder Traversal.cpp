/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        TreeNode *root = getTree(postorder, inorder, postorder.size() - 1, 0, inorder.size());
        return root;
    }
    
    TreeNode* getTree(vector<int> &postorder, vector<int> &inorder, int idx, int left, int right) {
        if (left >= right) return nullptr;
        
        TreeNode *root = new TreeNode(postorder[idx]);
        int middle = -1;
        for (int i = right - 1; i >= left; i--) {
            if (postorder[idx] == inorder[i]) {
                middle = i;
                break;
            }
        }
        if (middle >= 0) {
            root->right = getTree(postorder, inorder, idx - 1, middle + 1, right);
            root->left  = getTree(postorder, inorder, idx - (right - middle), left, middle);
        }
        return root;
    }
};