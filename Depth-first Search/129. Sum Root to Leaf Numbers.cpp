/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int sumNumbers(TreeNode* root) {
        int total = 0;
        DFS(root, 0, total);
        return total;
    }
    
    void DFS(TreeNode *root, int current, int &total) {
        if (!root) return;
        current = current * 10 + root->val;
        if (!root->left && !root->right) {
            total += current;
            return;
        }
        if (root->left) {
            DFS(root->left, current, total);
        }
        if (root->right) {
            DFS(root->right, current, total);
        }
    }
};