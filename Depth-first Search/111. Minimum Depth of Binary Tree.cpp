/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int m_ans = numeric_limits<int>::max();
    int minDepth(TreeNode* root) {
        if (!root) return 0;
        DFS(root, 1);
        return m_ans ;
    }
        
    void DFS(TreeNode *root, int depth) {
        if (!root) return;
        if (!root->left && !root->right) {
            m_ans = min(m_ans, depth);
            return;
        }
        
        DFS(root->left, depth + 1);
        DFS(root->right, depth + 1);
    }
};