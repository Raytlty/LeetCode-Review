class Solution {
public:
    int scheduleCourse(vector<vector<int>>& courses) {
        sort(begin(courses), end(courses), [](vector<int> &a, vector<int> &b){
            if (a[1] == b[1]) return a[0] < b[0];
            else return a[1] < b[1];
        });
        priority_queue<vector<int>, \
            vector<vector<int>>, \
            function<bool(vector<int> &a, vector<int> &b)>> que([](vector<int> &a, vector<int> &b) {
            if (a[0] == b[0]) return a[1] > b[1];
            return a[0] < b[0];
        });
        int duration = 0;
        for (vector<int> course: courses) {
            // cout << course[0] << " " << course[1] << " " << duration << endl;
            if (duration + course[0] <= course[1]) {
                duration += course[0];
                que.push(course);
            }
            else {
                // cout << que.top()[0] << " " << que.top()[1] <<endl;
                if (!que.empty() && course[0] < que.top()[0] &&\
                    duration - que.top()[0] + course[0] <= course[1]) {
                    duration = duration - que.top()[0] + course[0];
                    que.pop();
                    que.push(course);
                }
            }
        }
        return que.size();
    }
};