class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& findNums, vector<int>& nums) {
        vector<int> res;
        for (int x: findNums) {
            vector<int>::iterator it = find(nums.begin(), nums.end(), x);
            bool flag = true;
            for (;it != nums.end(); it++) {
                if (*it > x) {
                    flag = false;
                    res.push_back(*it);
                    break;
                }
            }
            if (flag) {
                res.push_back(-1);
            }
        }
        return res;
    }
};