class Solution {
public:
    int calPoints(vector<string>& ops) {
        stack<int> operations;
        int total = 0;
        for (string op: ops) {
            if (op == "+") {
                int last = operations.top(); operations.pop();
                int second = operations.top();
                total += last + second;
                operations.push(last);
                operations.push(last + second);
            }
            else if (op == "C") {
                int last = operations.top();operations.pop();
                total -= last;
            }
            else if (op == "D") {
                int doubleLast = operations.top() * 2;
                total += doubleLast;
                operations.push(doubleLast);
            }
            else if (is_digits(op)) {
                total += stoi(op);
                operations.push(stoi(op));
            }
        }
        return total;
    }
    
    bool is_digits(string op) {
        return op.find_first_not_of("-0123456789") == string::npos;
    }
};