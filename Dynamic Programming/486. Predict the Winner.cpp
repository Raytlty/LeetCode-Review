class Solution {
public:
    bool PredictTheWinner(vector<int>& nums) {
        vector<vector<int> > dp(nums.size(), vector<int>(nums.size(), 0));
        vector<int> sum(nums.size(), 0);
        if (nums.size() == 1) return true;
        for (int i = 0; i < (int)nums.size(); i++) {
            if (i == 0) {
                sum[i] = nums[i];
            }
            else {
                sum[i] = nums[i] + sum[i-1];
            }
            dp[i][i] =nums[i];
        }

        for (int range = 1; range < (int)nums.size(); range++) {
            for (int i = 0; i + range < (int)nums.size(); i++) {
                int j = i + range;
                int _sum;
                if (i == 0) {
                    _sum = sum[j];
                }
                else {
                    _sum = sum[j] - sum[i-1];
                }
                dp[i][j] = _sum - min(dp[i+1][j], dp[i][j-1]);
            }
        }

        // for (auto x: dp) {
        //     for (auto y : x) {
        //         cout << y << " ";
        //     }
        //     cout << endl;
        // }

        if (dp[0][nums.size() - 1] * 2 >= sum[nums.size() - 1]) return true;
        return false;
    }
};