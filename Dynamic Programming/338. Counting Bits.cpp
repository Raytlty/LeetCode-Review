class Solution {
public:
    vector<int> countBits(int num) {
        vector<int> dp(num + 1, 0);
        for (int i = 0; i <= num; i++) {
            if ((i & (-i)) == i) {
                dp[i] = 1;
            }
        }
        dp[0] = 0; dp[1] = 1;
        int limit = 0;
        for (int i = 2; i <= num; i++) {
            if (dp[i]) {
                limit = i;
                continue;
            }
            dp[i] = dp[limit] + dp[i-limit];
        }

        return dp;
    }
};