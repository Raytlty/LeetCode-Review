class Solution {
public:
    int numDecodings(string s) {
        vector<int> dp(s.size() + 1, 0);
        dp[0] = 1;
        dp[1] = (s[0] == '0'? 0: 1);
        for (unsigned i = 2; i <= s.size(); i++) {
            if (s[i - 1] == '0') {
                if (s[i - 2] == '1' || s[i - 2] =='2')
                    dp[i] = (i >= 2 ? dp[i - 2] : 1);
            }
            else if (s[i - 2] == '1' || (s[i - 2] == '2' && s[i - 1] <= '6' && s[i - 1] >= '1')) {
                dp[i] = dp[i - 2] + dp[i - 1];
            }
            else {
                dp[i] = dp[i - 1];
            }
        }
        // for (int i = 1; i <= s.size(); i++) {
        //     cout << dp[i] << " ";
        // }
        cout << endl;
        return dp[s.size()];
    }
};