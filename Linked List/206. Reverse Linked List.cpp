/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        return reverseFunc(head, nullptr);
    }
    
    ListNode *reverseFunc(ListNode *curr, ListNode *prev) {
        if (!curr) return prev;
        ListNode *node = reverseFunc(curr->next, curr);
        curr->next = prev;
        return node;
    }
};