/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        if (!head) return nullptr;
        while (head && head->val == val) head = head->next;
        ListNode *prev = nullptr, *curr = head;
        removeFunc(curr, prev, val);
        return head;
    }
    void removeFunc(ListNode *curr, ListNode *prev, int val) {
        if (!curr) return;
        if (curr->val == val) {
            prev->next = curr->next;
            removeFunc(curr->next, prev, val);
        }
        else 
            removeFunc(curr->next, curr, val);
    }
};