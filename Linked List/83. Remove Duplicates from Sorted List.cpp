/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if (!head) return nullptr;
        while (head && head->next) {
            if (head->val == head->next->val) {
                head = head->next;
            }
            else {
                head->next = deleteDuplicates(head->next);
                break;
            }
        }
        return head;
    }
};