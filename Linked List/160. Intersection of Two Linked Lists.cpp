/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int lengthA = getListLen(headA);
        int lengthB = getListLen(headB);
        if (lengthA > lengthB) {
            int len = lengthA - lengthB;
            while (len > 0) {
                headA = headA->next; len--;
            }
            while (headA && headB) {
                if (headA == headB) break;
                headA = headA->next; headB = headB->next;
            }
        }
        else {
            int len = lengthB - lengthA;
            while (len > 0) {
                headB = headB->next; len--;
            }
            while (headA && headB) {
                if (headA == headB) break;
                headA = headA->next; headB = headB->next;
            }
        }
        return headA;
    }
    
    int getListLen(ListNode *head) {
        int length = 0;
        while (head) {
            head = head->next;
            length++;
        }
        return length;
    }
};