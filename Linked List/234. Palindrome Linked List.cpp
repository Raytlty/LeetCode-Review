/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        ListNode *root = head;
        ListNode *node1 = createList(root);
        ListNode *node2 = reverseList(head, nullptr);
        while (node1 && node2) {
            if (node1->val != node2->val) return false;
            node1 = node1->next;
            node2 = node2->next;
        }
        return true;
    }
    ListNode *createList(ListNode *root) {
        if (!root) return nullptr;
        ListNode *node = new ListNode(root->val);
        node->next = createList(root->next);
        return node;
    }
    ListNode *reverseList(ListNode *curr, ListNode *prev) {
        if (!curr) return prev;
        ListNode *node = reverseList(curr->next, curr);
        curr->next = prev;
        return node;
    }
};