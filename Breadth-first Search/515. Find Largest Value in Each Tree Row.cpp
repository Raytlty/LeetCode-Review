/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> largestValues(TreeNode* root) {
        vector<int> results;
        if (!root) return results;
        vector<TreeNode*> vec;
        queue<TreeNode*> que;
        que.push(root);
        int minium = numeric_limits<int>::min();
        while (!que.empty()) {
            vec.clear();
            int maxium = minium;
            while (!que.empty()) {
                maxium = max(maxium, que.front()->val);
                vec.push_back(que.front());
                que.pop();
            }
            results.push_back(maxium);
            for (TreeNode *x: vec) {
                if (x->right)
                    que.push(x->right);
                if (x->left) 
                    que.push(x->left);
            }
        }
        return results;
    }
};