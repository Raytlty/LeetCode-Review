#include <iostream>
using namespace std;


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        vector<int> result;
        if (!root) return result;
        queue<TreeNode*> que;
        vector<TreeNode*> temp;
        que.push(root);
        while (!que.empty()) {
            temp.clear();
            while(!que.empty()) {
                temp.push_back(que.front());
                que.pop();
            }
            if (temp.size() > 0) {
                result.push_back(temp[temp.size() - 1]->val);
            }
            for (unsigned i = 0; i < temp.size(); i++) {
                if (temp[i]->left)
                    que.push(temp[i]->left);
                if (temp[i]->right)
                    que.push(temp[i]->right);
            }
        }
        return result;
    }
};
