// MLE
class Solution {
public:
    int swimInWater(vector<vector<int>>& grid) {
        int N = grid.size();
        int miniumn = numeric_limits<int>::max();
        int directed[][2] = {{1, 0}, {0, 1}, {0, -1}, {-1, 0}};
        queue<pair<int, int>> que;
        vector<vector<int>> used(N * N, vector<int>(N * N, miniumn));
        que.push(make_pair(0, grid[0][0]));
        while (!que.empty()) {
            auto s = que.front(); que.pop();
            int x = s.first / N;
            int y = s.first % N;
            if (x == N - 1 && y == N - 1) {
                miniumn = min(miniumn, s.second);
                continue;
            }
            if (grid[x][y] >= miniumn) continue;
            for (int i = 0; i < 4; i++) {
                int dx = x + directed[i][0];
                int dy = y + directed[i][1];
                if (dx >= 0 && dx < N && dy >= 0 && dy < N) {
                    int maxiumn = max(max(grid[x][y], grid[dx][dy]), s.second);
                    if (used[x * N + y][dx * N + dy] > maxiumn) {
                        que.push(make_pair(dx * N + dy, maxiumn));
                        used[x * N + y][dx * N + dy] = maxiumn;
                    }
                }
            }
        }
        return miniumn;
    }
}; 

// 527ms
class Solution {
public:
    int swimInWater(vector<vector<int>>& grid) {
        int direct[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        int N = static_cast<int>(grid.size());
        queue<int> que;
        vector<vector<bool>> used(N, vector<bool>(N, false));
        que.push(0);
        int maxiumn = 0;
        int x = -1, y = -1;
        vector<int> temp;
        while (!que.empty()) {
            temp.clear();
            int miniumn = N * N;
            while (!que.empty()) {
                int t = que.front(); que.pop();
                int tx = t / N, ty = t % N;
               
                if (used[tx][ty]) {
                    continue;
                }
                if (miniumn > grid[tx][ty]) {
                    x = tx, y = ty;
                    miniumn = grid[tx][ty];
                    cout << x << " " << y << " " << miniumn << endl;
                }
                temp.push_back(t);
            }
            maxiumn = max(maxiumn, miniumn);
            used[x][y] = true;
            if (x == N - 1 && y == N - 1) {
                break;
            }
            for (int d = 0; d < 4; d++) {
                int dx = direct[d][0] + x, dy = direct[d][1] + y;
                if (dx >= 0 && dx < N && dy >= 0 && dy < N && !used[dx][dy]) {
                    que.push(dx * N + dy);
                } 
            }
            for (int i = 0; i < temp.size(); i++) {
                que.push(temp[i]);
            }
        }
        return maxiumn;
    }
};

//27ms
class Solution {
public:
    int swimInWater(vector<vector<int>>& grid) {
        int direct[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        int N = static_cast<int>(grid.size()), maxiumn = 0;
        vector<vector<bool>> used(N, vector<bool>(N, false));
        priority_queue<pair<int, int>, vector<pair<int, int>>,\
                       function<bool(pair<int, int>, pair<int, int>)>> \
                           que([](pair<int, int> x, pair<int, int> y) {return x.first > y.first;});
        used[0][0] = true;
        que.push(make_pair(grid[0][0], 0));
        while (!que.empty()) {
            auto p = que.top(); que.pop();
            maxiumn = max(p.first, maxiumn);
            if (p.second / N == N - 1 && p.second % N == N - 1)
                break;
            used[p.second / N][p.second % N] = true;
            for (int d = 0; d < 4; d++) {
                int dx = p.second / N + direct[d][0];
                int dy = p.second % N + direct[d][1];
                if (dx >= 0 && dx < N && dy >= 0 && dy < N && !used[dx][dy])
                    que.push(make_pair(grid[dx][dy], dx * N + dy));
            }
        }
        return maxiumn;
    }
};