/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int findBottomLeftValue(TreeNode* root) {
        queue<TreeNode*> que;
        vector<TreeNode*> vec;
        TreeNode* result;
        que.push(root);
        while (!que.empty()) {
            vec.clear();
            while (!que.empty()) {
                vec.push_back(que.front());
                que.pop();
            }
            result = vec[vec.size() - 1];
            for (unsigned i = 0; i < vec.size(); i++) {
                if (vec[i]->right) {
                    que.push(vec[i]->right);
                }
                if (vec[i]->left) {
                    que.push(vec[i]->left);
                }
            }
        }
        return result->val;
    }
};