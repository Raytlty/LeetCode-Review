class Solution {
public:
    vector<string> letterCasePermutation(string S) {
        vector<string> results;
        results.push_back(S);
        for (int i = 0; i < S.size(); i++) {
            if ((S[i] >= 'a' && S[i] <= 'z')
                ||(S[i] >= 'A' && S[i] <= 'Z')) {
                char &ch = S[i];
                if (ch >= 'a' && ch <= 'z') {
                    ch = toupper(ch);
                    results.push_back(S);
                    DFS(i, S, results);
                    ch = tolower(ch);
                }
                else {
                    ch = tolower(ch);
                    results.push_back(S);
                    DFS(i, S, results);
                    ch = toupper(ch);
                }
            }
        }
        return results;
    }
        
    void DFS(int idx, string S, vector<string> &results) {
        
        for (int i = idx + 1; i < S.size(); i++) {
            if ((S[i] >= 'a' && S[i] <= 'z')
                ||(S[i] >= 'A' && S[i] <= 'Z')) {
                char &ch = S[i];
                if (ch >= 'a' && ch <= 'z') {
                    ch = toupper(ch);
                    results.push_back(S);
                    DFS(i, S, results);
                    ch = tolower(ch);
                }
                else {
                    ch = tolower(ch);
                    results.push_back(S);
                    DFS(i, S, results);
                    ch = toupper(ch);
                }
            }
        }
    }
};